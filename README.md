# PyNGAvatar
A GUI application to display a talking virtual avatar for streaming purposes.

## Features

PyNGAvatar is able to:

+ display a .png avatar idle while silent and talking while talking;
+ regulate the voice volume threshold above which the talking avatar activates using a simple slider;
+ switch the avatar background between three colors (green, blue and magenta) for easy window capture and transparency management.

## What do you need to run the program

The program needs:

- python3 interpreter
- tkinter (it should be installed with the python interpreter)
- pyaudio
    - on Debian, ```sudo apt install python3-pyaudio``` or ```python3 -m pip install pyaudio```
- pillow
    - on Debian,
    ```sudo apt install python3-pil python3-pil.imagetk``` or ```python3 -m pip install pillow```

## How to use it
The directory containing the program must contain the idle avatar named "idle.png" and the talking avatar named "talking.png".  
Buttons change the background, the slider changes the threshold above which the talking avatar activates.

### License
Please refer to the LICENSE file.
