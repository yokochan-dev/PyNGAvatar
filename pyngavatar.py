import pyaudio as pa
import audioop
import math
import os
import sys
import tkinter as tk
from PIL import Image, ImageTk
import time

def initialize_stream():
    chunk = 4096  # Record in chunks of 4096 samples
    sample_format = pa.paInt16  # 16 bits per sample
    channels = 2
    fs = 44100  # Record at 44100 samples per second
    return chunk, sample_format, channels, fs

def collect_input_devices(p):
    devices_info=p.get_host_api_info_by_index(0)
    devices_number=devices_info.get('deviceCount')
    input_devices=[]
    #print(devices_number)
    for i in range(0, devices_number):
        device={"index": 0, "name":"null"}
        device["index"]=p.get_device_info_by_host_api_device_index(0,i).get("index")
        device["name"]=p.get_device_info_by_host_api_device_index(0,i).get("name")
        if(p.get_device_info_by_host_api_device_index(0,i)).get("maxInputChannels")>0:
            input_devices.append(device)
    return input_devices

def create_canvas(root,idle,image_w,image_h):
    #Configuring and placing the canvas frame
    avatar_canvas=tk.Canvas(root, bg="green", height=image_h+50, width=image_w+100)
    avatar_canvas.create_image(40,40,anchor='center',image=idle,tags="image")
    avatar_canvas.grid(column=0,row=0)
    return avatar_canvas

def create_background_options(options_frame):
    background_options=('green','blue','magenta')
    background_options_var=tk.StringVar(value=background_options)
    bgselect=tk.Listbox(options_frame,listvariable=background_options_var )
    return bgselect

def create_slider(options_frame):
    slider=tk.Scale(options_frame,from_=0,to=100,orient='horizontal', bg='white')
    return slider

def avatar_canvas_update_img(canvas,img):
    canvas.delete("image")
    canvas.create_image(40,40,anchor='nw',image=img,tags="image")
    return

def change_background(color, avatar_canvas):
    avatar_canvas.configure(bg=color)
    return

def max_dim(a,b):
    aw, ah=a.size
    bw, bh=b.size
    w=max(aw,bw)
    h=max(ah,bh)
    return w, h

def open_stream():
    #input_devices=collect_input_devices()
    #if(input_devices.length()<=0):
        #return -1
    if(os.path.exists("idle.png")==False or os.path.exists("talking.png")==False):
        sys.exit("Make sure idle.png and talking.png exists!")
    #Opening images and creating root
    root=tk.Tk()
    root['bg']='white'
    idle_img=Image.open("idle.png")
    talking_img=Image.open("talking.png")
    idle=tk.PhotoImage(file="idle.png")
    talking=tk.PhotoImage(file="talking.png")
    image_w, image_h=max_dim(idle_img,talking_img)
    root.title("P(y)NGTuber")
    #print(image_w)
    #print(image_h)
    root.geometry(str(image_w+300)+'x'+str(image_h+50)+'+20+20')
    background_options=('green','blue','magenta')
    #Creating options frame
    options_frame=tk.Frame(root)
    options_frame['bg']='white'
    root.columnconfigure(0,weight=1)
    root.columnconfigure(1,weight=1)
    #Configuring and placing the canvas frame
    avatar_canvas=create_canvas(root,idle,image_w,image_h)
    avatar_canvas.grid(column=0,row=0,sticky='nw')
    options_frame.grid(column=1,row=0,sticky='n')
    #Configuring the background bgselect and its frame
    bg_frame=tk.Frame(options_frame)
    bg_frame['bg']='white'
    bg_frame['borderwidth']=1
    bgbuttons_label=tk.Label(bg_frame, anchor='center',
                          borderwidth=1, text="Select background color:",
                          relief='solid', padx=5, pady=5, bg='white')
    bgreen=tk.Button(bg_frame, text='green', command=lambda: change_background('green',avatar_canvas), bg='green')
    bblue=tk.Button(bg_frame, text='blue', command=lambda: change_background('blue',avatar_canvas), bg='blue')
    bmagenta=tk.Button(bg_frame, text='magenta', command=lambda: change_background('magenta',avatar_canvas), bg='magenta')
    bg_frame.grid(column=1,row=0)
    bgbuttons_label.pack(ipadx=30,ipady=30)
    bgreen.pack(side='left')
    bblue.pack(side='left')
    bmagenta.pack(side='left')
    #Create slider and frame
    slider_frame=tk.Frame(options_frame)
    slider_frame['bg']='white'
    slider_frame.grid(column=1,row=1)
    slider_volume_label=tk.Label(slider_frame, anchor='center',
                                 borderwidth=1, text="Voice volume threshold:",
                                 relief='solid', padx=10, pady=10, bg='white')
    slider=create_slider(slider_frame)
    slider_volume_label.pack()
    slider.pack()
    #Microphone logic
    p=pa.PyAudio()
    #See if there are input devices_info
    input_devices=collect_input_devices(p)
    if(len(input_devices)==0):
        sys.exit("There are no input devices!")
    chunk,sample_format,channels,fs=initialize_stream()
    stream = p.open(format=sample_format, channels=channels, rate=fs, frames_per_buffer=chunk, input=True)
    while(True):
        root.update_idletasks()
        root.update()
        try:
            threshold=slider.get()
            data=stream.read(chunk)
            volume=audioop.rms(data,2)
            if(volume>=1):
                db=20*math.log(volume,10)
            else:
                db=0
            #print(volume)
            #print(db)
            if(db>=threshold):
                avatar_canvas_update_img(avatar_canvas,talking)
            else:
                avatar_canvas_update_img(avatar_canvas,idle)
        except KeyboardInterrupt:
            data=stream.close()
            break

open_stream()
